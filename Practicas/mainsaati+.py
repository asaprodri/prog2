import clasessaati as saati

OrderUni = saati.Order("1", "Uni", "cotton", 2, 11, 2, 2, 1, 10)
Sheet1 = saati.FabricSheet("1", "cotton", 110, 110, "department_1", True)
Piece1 = saati.StockPiece("1", "normal", 11, 11, 10, 8)
Sheet1.maps["Initial"].addPiece(Piece1)
OrderUni.addPiece(Piece1)

print(f'The total area is {OrderUni.areaOfOrder()}.')


orderRodrigo = saati.Order("2", "Rodrigo", "corduroy", 2, 16, 2, 2, 2, 12)
Sheet1 = saati.FabricSheet("2", "corduroy", 110, 110, "department_2", True)
Piece1 = saati.StockPiece("2", "normal", 16, 13, 2, 2)
Sheet1.maps["Initial"].addPiece(Piece1)
orderRodrigo.addPiece(Piece1)

print(f'The total area is {orderRodrigo.areaOfOrder()}.')


orderJuan = saati.Order("3", "Juan", "linen", 2, 18, 2, 3, 2, 13)
Sheet1 = saati.FabricSheet("3", "linen", 110, 110, "department_3", True)
Piece1 = saati.StockPiece("3", "normal", 14, 12, 8, 6)
Sheet1.maps["Initial"].addPiece(Piece1)
orderJuan.addPiece(Piece1)

print(f'The total area is {orderJuan.areaOfOrder()}.')

