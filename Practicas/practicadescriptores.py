"""
Rodrigo Santolaya Gil
6/11/2020
Programación II
"""

import sqlite3
from email_validator import validate_email, EmailNotValidError

conn = sqlite3.connect('users.db')

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS agenda_contactos (id not null primary 
key, name, surname, email, phone1, phone2, age)''')

"""# Insert a row of data
c.execute(
    "INSERT INTO agenda_contactos VALUES (123, 'rodrigo', 'santolaya', "
    "'rodrisanto@gmail.com ', '+6123456781', '+6123456781', 21)")
c.execute(
    "INSERT INTO agenda_contactos VALUES (1234, 'juanjo', 'nieto', "
    "'jnieto@gmail.com ', '+600987657','+600987657', 45)")
c.execute(
    "INSERT INTO agenda_contactos VALUES (12345, 'alvaro', 'martos', "
    "'amartos1@gmail.com ', '+677853456','+677853456', 19)")
"""

c.execute("SELECT * FROM agenda_contactos ")
print(c.fetchall())



class validarEmail:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):
        try:
            valido = validate_email(value)
            value = valido.email
            conn.execute(self.store, [value, instance.key])
            conn.commit()

        except EmailNotValidError as e:
            print('Value error: inappropriate email')


class validarTelefono:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):

        try:
            Valor = value[:0]
            for i in value:
                if Valor == '+' and 8 <= len(value) <= 12:
                    conn.execute(self.store, [value, instance.key])
                    conn.commit()

        except ValueError:
            print('Error, Porfavor introduzca un número de teléfono valido')


class validarEdad:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):

        try:
            if 0 < value < 130:
                conn.execute(self.store, [value, instance.key])
                conn.commit()

        except ValueError:
            print('Error, Porfavor introduzca la edad correcta')


class rest:

    def __set__name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):
        conn.execute(self.store, [value, instance.key])
        conn.commit()


class user:
    table = 'agenda_contactos '
    key = 'id'
    name = rest()
    surname = rest()
    email = validarEmail()
    phone_1 = validarTelefono()
    phone_2 = validarTelefono()
    age = validarEdad()

    def init(self, key):
        self.key = key

'''------------------------------------------'''
rodrigo = user(123)
try:
    assert rodrigo.age == 21
except AssertionError:
    print('edad incorrecta')

try:
    rodrigo.age = 17
except ValueError:
    print('edad incorreta')

try:
    assert rodrigo.age == 17
except AssertionError:
    print('edad incorrecta')

try:
    rodrigo.age = 79
except ValueError:
    print('edad incorrecta')

'''------------------------------------------'''
juanjo = user(1234)
try:
    assert juanjo.phone_1 == '+600987657'
except AssertionError:
    print('numero incorrecto')

try:
    juanjo.phone_1 = '+675379765'
except ValueError:
    print('numero incorrecto')

try:
    assert juanjo.phone_1 == '+675379765'
except AssertionError:
    print('numero incorrecto')

try:
    juanjo.phone_1 = '+6987764578878'
except ValueError:
    print('numero incorrecto')

'''------------------------------------------'''
alvaro = user(12345)
try:
    assert alvaro.email == 'amartos1@gmail.com'
except AssertionError:
    print('email erroneo')

try:
    alvaro.email = 'amartos123@gmail.com'
except ValueError:
    print('email erroneo')

try:
    assert alvaro.email == 'amartos123@gmail.com'
except AssertionError:
    print('email erroneo')

try:
    alvaro.email = 'amartose3434gmail.com '
except ValueError:
    print('email erroneo')


conn.commit()
conn.close()

