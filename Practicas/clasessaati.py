
class StockPiece:

    def __init__(self, id_stock_piece, quality_level, height, length,
               position_x, position_y):

        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y

    def get_area(self) -> float:
        return self.height * self.length


class Order:

    def __init__(self, id_order, customer, raw_material,
               min_height: float,
               max_height: float, min_quantity: float, max_quantity: float,
               min_cut_length: float, max_cut_length: float):
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length
        self.stockpieces = set([])

    def addPiece(self, stockpiece: StockPiece):
        self.stockpieces.add(stockpiece)

    def areaOfOrder(self) -> float:
        return sum([stockpiece.get_area() for stockpiece in self.stockpieces])


class Map:

    def __init__(self, maptype, real):
        self.maptype = maptype
        self.real = real
        self.assigned_stockpieces = set([])

    def addPiece(self, stockpiece: StockPiece):
        self.assigned_stockpieces.add(stockpiece)


class FabricSheet:

    def __init__(self, id_fabric_sheet, raw_material: str, height: float,
               length: float, department: str, weaving_forecast: bool):
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast

        self.maps = {}
        self.add_map(Map("Initial", weaving_forecast))
        self.add_map(Map("Intermediate", weaving_forecast))
        self.add_map(Map("Final", weaving_forecast))

    def add_map(self, input_map: Map):
        self.maps[input_map.maptype] = input_map
